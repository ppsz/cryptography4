#include "Header.h"

unsigned long long ModularExponentiation(unsigned long long base, unsigned long long exponent, unsigned long long modulo)
{
	unsigned long long result = 1;
	std::bitset<128> bit_exponent(exponent);
	base %= modulo;

	for (int i = 0; i < 128; i++)
	{
		if (bit_exponent[i] == 1)
		{
			result *= base;
			result %= modulo;
		}
		base *= base;
		base %= modulo;
	}

	return result;
}
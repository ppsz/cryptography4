#include "Header.h"

bool MillerRabinTest(unsigned long long number, int accuracy)
{
	std::array<unsigned long long, 31> power_of_2 = {
		1, 2,  4,  8,  16,  32,  64,
		128,  256,  512,  1024, 2048, 4096, 8192,
		16384, 32768, 65536,131072, 262144, 524288, 1048576,
		2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728,
		268435456, 536870912, 1073741824 };
	std::random_device rd;
	std::mt19937 mt(1337);
	std::uniform_int_distribution<unsigned long long> dist(1, number);
	unsigned long long max_power, power, random_number;

	if(number < 2)
		return false;
	if (number<4)
		return true;
	if (number % 2 == 0)
		return false;

	for (int i = 0; i < 31; ++i)
		if (number - 1 % power_of_2[i] != 0)
			max_power = i;
	power = number / std::pow(2, max_power);

	for (int i = 0; i<accuracy; ++i)
	{
		random_number = dist(mt);
		if (ModularExponentiation(random_number, power, number) != 1)
			for (int j = 0; j < max_power; ++j)
				if (ModularExponentiation(random_number, power_of_2[j] * power, number) != number - 1)
					return false;
	}
	return true;
}
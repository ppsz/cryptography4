#pragma once
#include <iostream>
#include <bitset>
#include <array>
#include <vector>
#include <random>
#include <cmath>


unsigned long long ModularExponentiation(unsigned long long base, unsigned long long exponent, unsigned long long modulo);
bool MillerRabinTest(unsigned long long number, int accuracy);